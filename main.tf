provider "aws" {
    region = var.region
}

variable "region" {
  type        = string
  default     = "ca-central-1"
  description = "region for the resources to be created on"
}

variable "availability_zone" {
  type        = string
  default     = "ca-central-1a"
  description = "az for the resource to be created on"
}

variable vpc_cidr_block {
  type        = string
  default     = ""
  description = "description"
}

variable subnet_cidr_block {
  type        = string
  default     = ""
  description = "description"
}

variable env_prefix {
  type        = string
  description = "type of environment we are workign on"
}

variable my_ip {
  type        = string
  description = "user ip address to be allowed to get access to ssh into servers(nginx)"
}

variable instance_type {
  type        = string
  default     = "t2.micro"
  description = "instance type of EC2 to be created for servers"
}

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    cidr_block = var.subnet_cidr_block
    vpc_id = aws_vpc.myapp-vpc.id
    availability_zone = var.availability_zone
    tags = {
        Name = "${var.env_prefix}-subnet"
    }
}

resource "aws_route_table" "myapp-rtb" {
    vpc_id = aws_vpc.myapp-vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name = "${var.env_prefix}-rtb"
    }
}

resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    tags = {
        Name = "${var.env_prefix}-igw"
    }
}

resource "aws_route_table_association" "a-rtb-subnet" {
    route_table_id = aws_route_table.myapp-rtb.id
    subnet_id = aws_subnet.myapp-subnet-1.id
}

resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }
    
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
        Name = "${var.env_prefix}-sg"
    }
}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]

    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

# output "aws_ami" {
#     value = data.aws_ami.latest-amazon-linux-image.id
# }

resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg.id]
    availability_zone = var.availability_zone

    associate_public_ip_address = true
    key_name = "server-key-pair"

    tags = {
        Name = "${var.env_prefix}-server"
    }
}